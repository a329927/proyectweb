//Constantes
const log4js = require('log4js');
//Var se define como una variable global y puede utilizarse en cualquier parte 
let logger = log4js.getLogger();
//Configura el nivel que quieres dentro de tu aplicacion por medio de una jerarquia
logger.level="debug";

//En este orden se ejecutan
logger.info("La aplicacion se ejecuto correctamente.");
logger.warn("Cuidado, falta un archivo de configuaracion.")
logger.error("No se encontro la funcion email");

//Va a imprimirse solo si la aplicacion no funciona
logger.fatal("La aplicacion no se pudo iniciar");

//funcion
function sumar(x,y){
	return x + y;
}
//Como establecer acceso
module.exports = sumar;
