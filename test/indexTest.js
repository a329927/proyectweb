const sumar = require("../index");
const assert = require("assert");
//old -> caja negra y caja blanca
//today -> pruebas unitarias (Mantener las clases muy pequeñas u ordenadas
//		-> pruebas funcionales (E2E)
//			-> Cobertura - Coverage(Cantidad de codigo que cubres en tus pruebas funcionales)
//		-> Pruebas de integracion (Se utiliza para revisar que un componente pueda comunicarse con otro)
//		-> Pruebas de estres (o concurrentes) -Sirven para entender los limites que una app puede aguantar-

//Como de escribe una prueba?
/*Siempre se hacen al 50% => Buscar que la prueba nos de lo que estamos buscando y 
tambien que no nos de lo que no estamos buscando.
Escribir una prueba correcta y una que falle
*/
//Asserts = Afirmacion

describe("Probar la suma de dos numeros", ()=>/*Funcion flecha o funcion anonima*/{
	//Afirmamos que 5 + 5 es 10
	it("5 + 5 es 10", ()=>{
		assert.equal(10, sumar(5,5));
	});
	//Afirmamos qye 5 + 5 no son 55
	it('5 + 5 no son 55',()=>{
		assert.equal(10, sumar(5,5));
	})
});